/*//////////////////////////////////////////////////////////////////////////////////////////////////
Name: Spencer Peterson
Date: 24 January, 2018
Assignment: ObjectAllocator
Course: CS280
Term: Sprint 2018
//////////////////////////////////////////////////////////////////////////////////////////////////*/ 
#include "ObjectAllocator.h"
#include <cstring>


// Creates the ObjectManager per the specified values
// Throws an exception if the construction fails. (Memory allocation problem)
ObjectAllocator::ObjectAllocator(size_t ObjectSize, const OAConfig &config)
{
  // Createa a config and a stats
  config_ = config;
  stats_.ObjectSize_ = ObjectSize;
  stats_.FreeObjects_ = config_.ObjectsPerPage_;

  stats_.ObjectsInUse_ = 0;

  // Create the first page
  try {
    SetupPage(CreatePage());
  }
  catch (OAException error){
    std::cout << error.code() << " : " << error.what();
    return;
  }
}

// Destroys the ObjectManager (never throws)
ObjectAllocator::~ObjectAllocator()
{

}





// Take an object from the free list and give it to the client (simulates new)
// Throws an exception if the object can't be allocated. (Memory allocation problem)
void *ObjectAllocator::Allocate(const char *label)
{
  /// Change this so that it can take into consideration that there are multiple pages
  if (stats_.FreeObjects_ <= 0)
  {
    if (stats_.PagesInUse_ >= config_.MaxPages_)
    {
      throw OAException(OAException::E_NO_PAGES, "There is no virtual memory left");
    }
    else
    {
      SetupPage(CreatePage());
      stats_.FreeObjects_ = config_.ObjectsPerPage_;
    }
  }

  GenericObject* newObject = FreeList_;
  FreeList_ = FreeList_->Next;

  stats_.Allocations_++;
  stats_.ObjectsInUse_++;
  stats_.FreeObjects_--;
  stats_.MostObjects_++;

  memset(reinterpret_cast<char*>(newObject) + sizeof(void*), ALLOCATED_PATTERN, stats_.ObjectSize_ - sizeof(void*));

  if (config_.HBlockInfo_.type_ == OAConfig::hbBasic)
  {
    *(reinterpret_cast<char*>(newObject) - config_.PadBytes_ - config_.HBlockInfo_.size_) = static_cast<char>(stats_.Allocations_);
    *(reinterpret_cast<char*>(newObject) - config_.PadBytes_ - 1) = 0x01;
  }
  else if (config_.HBlockInfo_.type_ == OAConfig::hbExtended)
  {
    *(reinterpret_cast<char*>(newObject) - config_.PadBytes_ - 1) = 0x01;
    *(reinterpret_cast<char*>(newObject) - config_.PadBytes_ - 5) = static_cast<char>(stats_.Allocations_);
    int temp = static_cast<int>(*(reinterpret_cast<char*>(newObject) - config_.PadBytes_ - 7));
    temp++;
  }
  else if (config_.HBlockInfo_.type_ == OAConfig::hbExternal)
  {
    char* otherTemp = reinterpret_cast<char*>(newObject) - config_.PadBytes_ - config_.HBlockInfo_.size_;

    
      MemBlockInfo* newMemBlock = new MemBlockInfo();
      newMemBlock->alloc_num = stats_.Allocations_;
      newMemBlock->in_use = true;


      char* newLabel;
      newLabel = new char[strlen(label)];
      strcpy(newLabel, label);
      newMemBlock->label = newLabel;



      void** temp = reinterpret_cast<void**>(otherTemp);
      *temp = newMemBlock;
    

  }



  return newObject;
}

// Returns an object to the free list for the client (simulates delete)
// Throws an exception if the the object can't be freed. (Invalid object)
void ObjectAllocator::Free(void *Object)
{
  // Check to see if this has already been freed
  if ((*(reinterpret_cast<char*>(Object) + sizeof(void*)) & 0xCC) == 0xCC)
    throw OAException(OAException::E_MULTIPLE_FREE, "Double Freeing");


  if(FindIfOnBoundary(Object))
    throw OAException(OAException::E_BAD_BOUNDARY, "Bad Boundary");
  //FindIfOnBoundary(Object);

  if (config_.HBlockInfo_.type_ == OAConfig::hbBasic)
  {
    *(reinterpret_cast<char*>(Object) - config_.PadBytes_ - config_.HBlockInfo_.size_) = 0x00;
    *(reinterpret_cast<char*>(Object) - config_.PadBytes_ - 1) = 0x00;
  }
  else if (config_.HBlockInfo_.type_ == OAConfig::hbExtended)
  {
    *(reinterpret_cast<char*>(Object) - config_.PadBytes_ - 1) = 0x00;
    *(reinterpret_cast<char*>(Object) - config_.PadBytes_ - 5) = 0x00;
  }
  else if (config_.HBlockInfo_.type_ == OAConfig::hbExternal)
  {
    char* temp = reinterpret_cast<char*>(Object) - config_.PadBytes_ - config_.HBlockInfo_.size_;
    memset(temp, 0, sizeof(void*));
  }

  GenericObject* freedObject = reinterpret_cast<GenericObject*>(Object);
  freedObject->Next = FreeList_;
  FreeList_ = freedObject;

  memset(reinterpret_cast<char*>(freedObject) + sizeof(void*), FREED_PATTERN, stats_.ObjectSize_ - sizeof(void*));
  stats_.FreeObjects_++;
  stats_.Deallocations_++;
  stats_.ObjectsInUse_--;
}

// Calls the callback fn for each block still in use
unsigned ObjectAllocator::DumpMemoryInUse(DUMPCALLBACK fn) const
{
  (void)fn;
  return 0;
}

// Calls the callback fn for each block that is potentially corrupted
unsigned ObjectAllocator::ValidatePages(VALIDATECALLBACK fn) const
{
  (void)fn;
  return 0;
}


// Frees all empty pages (extra credit)
unsigned ObjectAllocator::FreeEmptyPages(void)
{
  return 0;
}

// Returns true if FreeEmptyPages and alignments are implemented
bool ObjectAllocator::ImplementedExtraCredit(void)
{
  return false;
}



// true=enable, false=disable
void ObjectAllocator::SetDebugState(bool State)
{
  config_.DebugOn_ = State;
}

// returns a pointer to the internal free list
const void *ObjectAllocator::GetFreeList(void) const
{
  return FreeList_;
}

// returns a pointer to the internal page list
const void *ObjectAllocator::GetPageList(void) const
{
  return PageList_;
}

// returns the configuration parameters
OAConfig ObjectAllocator::GetConfig(void) const
{
  return config_;
}
// returns the statistics for the allocator
OAStats ObjectAllocator::GetStats(void) const
{
  return stats_;
}





// allocates another page of objectss
 char* ObjectAllocator::CreatePage(void)
{
   if (stats_.PagesInUse_ == 0)
   {
     // Calcuate the alignment blocks for the first and the inner
     if (config_.Alignment_)
     {
       config_.LeftAlignSize_ = static_cast<unsigned int>((sizeof(GenericObject*) + config_.PadBytes_ + config_.HBlockInfo_.size_) % config_.Alignment_);
       config_.InterAlignSize_ = static_cast<unsigned int>((stats_.ObjectSize_ + (2 * config_.PadBytes_) + config_.HBlockInfo_.size_) % config_.Alignment_);
     }
     else
     {
       config_.LeftAlignSize_ = static_cast<unsigned int>(0);
       config_.InterAlignSize_ = static_cast<unsigned int>(0);
     }

     sizeOfFirstBlock_ = static_cast<unsigned int>((sizeof(GenericObject*) + config_.PadBytes_ + config_.HBlockInfo_.size_) + config_.LeftAlignSize_);
     sizeOfInnerBlock_ = static_cast<unsigned int>(((stats_.ObjectSize_ + (2 * config_.PadBytes_) + config_.HBlockInfo_.size_) + config_.InterAlignSize_));

     // But first check to see if the objects has been specified
     if (config_.ObjectsPerPage_ == 0) objectsPerPage_ = DEFAULT_OBJECTS_PER_PAGE;
     else                             objectsPerPage_ = config_.ObjectsPerPage_;

     // Calcuate the entier page size by taking the size of the first block, plus the size of each block times the total number of blocks
     int pageSize = static_cast<int>(sizeOfFirstBlock_ + (sizeOfInnerBlock_ * objectsPerPage_) - config_.PadBytes_ - config_.HBlockInfo_.size_);
     stats_.PageSize_ = pageSize;


     // Allocate the memory for the new page
     char* newPage = new char[pageSize];

     // Check to see if new worked
     if (!newPage) throw OAException(OAException::E_NO_MEMORY, "Object Allocator Constructor has failed to allocate memory");

     // Increase the current page count
     ++currentPageCount_;

     // Point to the front of the list aned set next to null
     PageList_ = reinterpret_cast<GenericObject*>(newPage);
     PageList_->Next = nullptr;
     FreeList_ = nullptr;
     stats_.PagesInUse_++;

     return newPage;
   }
   else
   {
     stats_.PagesInUse_++;
     char* newPage = new char[stats_.PageSize_];
     GenericObject* temp = reinterpret_cast<GenericObject*>(newPage);
     if (!newPage) throw OAException(OAException::E_NO_MEMORY, "Object Allocator Constructor has failed to allocate memory");
     temp->Next = PageList_;
     PageList_ = temp;

     return newPage;
   }

}

void ObjectAllocator::SetupPage(char* pagePointer)
{
  /// PUT LOGIC TO HANDLE MULTIPLE PAGES ///

  // Create the first block of memory
  char* newBlock = pagePointer + sizeOfFirstBlock_;
  reinterpret_cast<GenericObject*>(newBlock)->Next = NULL;

  // Create the remaining blocks of memory
  GenericObject* previous = reinterpret_cast<GenericObject*>(newBlock);
  for (unsigned i = 1; i < objectsPerPage_; ++i)
  {
    GenericObject* newBlock = reinterpret_cast<GenericObject*>(pagePointer + sizeOfFirstBlock_ +  (sizeOfInnerBlock_ * i));
    newBlock->Next = previous;
    previous = newBlock;
    FreeList_ = newBlock;
  }

  // Initialize all of the memory for the currently made page
  SetupMemorySignatures(pagePointer);
}

void ObjectAllocator::SetupMemorySignatures(char* pagePointer)
{

  // Set up the signatures of the first block
  memset(pagePointer + sizeof(void*), ALIGN_PATTERN, config_.LeftAlignSize_);
  memset(pagePointer + sizeof(void*) + config_.LeftAlignSize_, 0x00, config_.HBlockInfo_.size_);
  memset(pagePointer + sizeof(void*) + config_.LeftAlignSize_ + config_.HBlockInfo_.size_, PAD_PATTERN, config_.PadBytes_);

  // Go through and set up the rest of the objects in the page
  for (unsigned int i = 0; i < objectsPerPage_; ++i)
  {
    // Set up that this has been unallocated
    memset(pagePointer + sizeOfFirstBlock_ + sizeof(void*)  + (i * sizeOfInnerBlock_), UNALLOCATED_PATTERN, stats_.ObjectSize_ - sizeof(void*));

    // Set up the frist set of pad bytes
    memset(pagePointer + sizeOfFirstBlock_ + sizeof(void*) + (i * sizeOfInnerBlock_) + stats_.ObjectSize_ - sizeof(void*), PAD_PATTERN, config_.PadBytes_);

    // Set up the alignment
    memset(pagePointer + sizeof(void*) + (sizeOfFirstBlock_ + (i * sizeOfInnerBlock_) + stats_.ObjectSize_ - sizeof(void*) + config_.PadBytes_), ALIGN_PATTERN, config_.InterAlignSize_);

    // Set up the alignment
    memset(pagePointer + +sizeof(void*) + (sizeOfFirstBlock_ + (i * sizeOfInnerBlock_) + stats_.ObjectSize_ - sizeof(void*) + config_.PadBytes_ + config_.InterAlignSize_), 0x00, config_.HBlockInfo_.size_);

    // Set up the last of the padding, which is after the header
    memset(pagePointer + +sizeof(void*) + (sizeOfFirstBlock_ + (i * sizeOfInnerBlock_) + stats_.ObjectSize_ - sizeof(void*) + config_.PadBytes_ + config_.InterAlignSize_ + config_.HBlockInfo_.size_), PAD_PATTERN, config_.PadBytes_);
  }
}

bool ObjectAllocator::FindIfOnBoundary(void* Object)
{
  (void)Object;

  return false;
}

// Make private to prevent copy construction and assignment
ObjectAllocator::ObjectAllocator(const ObjectAllocator &oa)
{
  (void)oa;
}

ObjectAllocator &ObjectAllocator::operator=(const ObjectAllocator &oa)
{
  (void)oa;
  return *this;
}


