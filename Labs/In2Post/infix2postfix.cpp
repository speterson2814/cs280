/*//////////////////////////////////////////////////////////////////////////////////////////////////
Name: Spencer Peterson
Date: 22 January, 2018
Assignment: In2Fix
Course: CS280
Term: Sprint 2018
//////////////////////////////////////////////////////////////////////////////////////////////////*/
#include "infix2postfix.h"
#include <string>
#include <iostream>
#include <stack>

int GetPrecedence(char c)
{
  // Based on the passed character, find the presendence of the value
  if(c == '*' || c == '/')
    return 2;
  else if (c == '+' || c == '-')
    return 1;

  return 0;
}

int SmallEval( int operand1, int operand2, int operate)
{
  switch (operate)
  {
    case '*': return operand2 * operand1;
    case '/': return operand2 / operand1;
    case '-': return operand2 - operand1;
    case '+': return operand2 + operand1;
    default:  return 0;
  }
}

int Evaluate(std::string const &postfix) 
{
  // Create a stack and a local variable for evalutaion
  std::stack<int> myStack;
  int finalValue = 0;

  // Go through the entire string
  for(unsigned i = 0; i < postfix.size(); ++i)
  {
    // If an operand is found, push it to the stack
    if (postfix[i] >= '0' && postfix[i] <= '9') myStack.push(postfix[i] - '0');
    // If an operator was found 
    else
    {
      // Record the two operands before this operator and remove them
      int operand1 = myStack.top();
      myStack.pop();
      int operand2 = myStack.top();
      myStack.pop();

      // Evaluate the last two values from the stack using the current operator 
      finalValue = SmallEval(operand1, operand2, postfix[i]);

      // Push back the value to use for the next calculation
      myStack.push(finalValue);
    }
  }

  // The final value was the last thing we found, which is the final value
  return finalValue;
}

/* Description: Grab a string, break it up, and evaluate that expression
 *
 */ 
std::string Infix2postfix(std::string const &postfix) 
{
  std::stack<char> myStack;
  std::string output;

  // Go though the postfix string
  for(unsigned i = 0; i < postfix.size(); ++i)
  {
    // If the character is an operand, output it
    if (postfix[i] >= '0' && postfix[i] <= '9') output += postfix[i];

    // If the scanned character is an '(', push it to the stack
    else if(postfix[i] == '(') myStack.push('(');

    // If the scanned character is a ')' pop and output from the stack
    // until an '(' is encountered
    else if(postfix[i] == ')')
    {
      while(!myStack.empty() && myStack.top() != '(')
      {
        output += myStack.top();
        myStack.pop();
      }

      // Check to see if we hit another '(')
      if(myStack.top() == '(') myStack.pop();
    }

    // If we found an operator
    // Pop the operator from the stack until the precedence of the scanned
    // operator  is less-equal to the precedence of the operator
    // on top of the stack. Push the scanned operator to the stack
    else 
    {
      while(!myStack.empty() && GetPrecedence(postfix[i]) <= GetPrecedence(myStack.top()))
      {
        output += myStack.top();
        myStack.pop();
      }

      myStack.push(postfix[i]);
    }
  }


  // Pop and output from the stack until it is not empty
  while(!myStack.empty())
  {
    output += myStack.top();
    myStack.pop();
  }

  return output;
}

